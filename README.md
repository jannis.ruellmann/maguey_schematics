# maguey_schematics

KiCad project sources for ESP32-C3-WROOM Maguey project.

# Introduction

Welcome to the maguey project!

Purpose of the project: Create a esp32 based project. 

Issues or Features can contributed by sending a mail to:
incoming+jannis-ruellmann-maguey-schematics-31652963-9dj6llkw8gs4p9lqwd81dn1s8-issue@incoming.gitlab.com



Main features of the board:

## External connectors
Maguey uses a USB-C type connector for charging. Following connector is used: https://www.sparkfun.com/products/15100
The breakout board can be connected to **J2**.

To program the board Connector **J1** is used. An external UART driver is necessary to program the board.
Please also provide 5V for the board. Following 

- 1 - RXD
- 2 - TXD
- 3 - VCC (5V)
- 4 - GND 

## Microcontroller
Used Microcontroller: ESP32-C3-WROOM-02
To switch buttons are available:
S1: BOOT   
S2: RESET  
All other available pins can be found on the J3 and J4.

## Voltages

To get the 3.3V the synchronous step-Down Regulator TD6817 is used. This allows a very efficient and 3.3V support.
Following voltages are available from the board:
- 3.7-5V on Pin_10 of J4 depending on if usb cable is connected or battery is used
- 3.3V on Pin_1 of J3

## Battery management
It is also possible, yet optional, to connect a single cell Lipo battery to the module. The board contains a Battery-Management IC
For that, the MCP73833 is used. 
