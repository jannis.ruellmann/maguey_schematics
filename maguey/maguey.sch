EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Maguey"
Date "2021-12-19"
Rev "V0.6"
Comp ""
Comment1 "Author: Jannis Ruellmann"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3400 2350 3000 2350
Connection ~ 3400 2350
Wire Wire Line
	3400 2300 3400 2350
Wire Wire Line
	3000 2350 2500 2350
Connection ~ 3000 2350
Wire Wire Line
	3000 2300 3000 2350
Connection ~ 2500 2350
Wire Wire Line
	2500 2250 2500 2350
Wire Wire Line
	2500 2350 2500 2450
Wire Wire Line
	4100 2350 3400 2350
$Comp
L power:GND #PWR02
U 1 1 619CC4D9
P 2500 2450
F 0 "#PWR02" H 2500 2200 50  0001 C CNN
F 1 "GND" H 2505 2277 50  0000 C CNN
F 2 "" H 2500 2450 50  0001 C CNN
F 3 "" H 2500 2450 50  0001 C CNN
	1    2500 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 800  3400 800 
$Comp
L Device:L L1
U 1 1 619BFFC9
P 2750 800
F 0 "L1" V 2940 800 50  0000 C CNN
F 1 "4.7uH" V 2849 800 50  0000 C CNN
F 2 "inductor_smd:L_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2750 800 50  0001 C CNN
F 3 "~" H 2750 800 50  0001 C CNN
	1    2750 800 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1200 1850 950  1850
Connection ~ 1200 1850
Wire Wire Line
	1200 1450 1200 1850
Wire Wire Line
	950  1850 950  1900
Connection ~ 950  1850
Wire Wire Line
	1550 1850 1200 1850
Wire Wire Line
	1550 1250 1550 1850
Wire Wire Line
	2000 1250 1550 1250
Wire Wire Line
	950  1450 950  1850
Wire Wire Line
	3400 1050 3400 1250
Connection ~ 3400 1050
Wire Wire Line
	3400 1050 3750 1050
Connection ~ 3400 1250
Wire Wire Line
	3400 800  3400 1050
Wire Wire Line
	1850 800  2600 800 
Wire Wire Line
	1850 1350 1850 800 
Wire Wire Line
	2000 1350 1850 1350
Wire Wire Line
	3000 1450 3000 1900
Wire Wire Line
	2500 1450 2500 1150
Connection ~ 2500 1450
Wire Wire Line
	2700 1450 2500 1450
Connection ~ 2500 1900
Wire Wire Line
	2500 1150 2350 1150
Wire Wire Line
	2500 1900 2500 1450
Wire Wire Line
	2500 1900 2500 2050
Wire Wire Line
	2750 1900 2500 1900
Connection ~ 3000 1900
Wire Wire Line
	2950 1900 3000 1900
Wire Wire Line
	3400 1900 3400 1250
Connection ~ 3400 1900
Wire Wire Line
	3000 1900 3400 1900
Wire Wire Line
	3000 2000 3000 1900
Wire Wire Line
	3400 2000 3400 1900
$Comp
L Device:R_Small R1
U 1 1 6198D41D
P 2500 2150
F 0 "R1" H 2559 2196 50  0000 L CNN
F 1 "150k" H 2559 2105 50  0000 L CNN
F 2 "resistor_smd:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 2500 2150 50  0001 C CNN
F 3 "~" H 2500 2150 50  0001 C CNN
	1    2500 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 61986B45
P 2850 1450
F 0 "C3" V 2900 1350 50  0000 C CNN
F 1 "22pF" V 2800 1300 50  0000 C CNN
F 2 "capacitor_smd:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 2888 1300 50  0001 C CNN
F 3 "~" H 2850 1450 50  0001 C CNN
	1    2850 1450
	0    1    1    0   
$EndComp
$Comp
L Device:C C4
U 1 1 6197C096
P 3000 2150
F 0 "C4" V 3150 2150 50  0000 C CNN
F 1 "10uF" V 2850 2150 50  0000 C CNN
F 2 "capacitor_smd:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 3038 2000 50  0001 C CNN
F 3 "~" H 3000 2150 50  0001 C CNN
	1    3000 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 6197B050
P 3400 2150
F 0 "C5" V 3550 2150 50  0000 C CNN
F 1 "10uF" V 3250 2150 50  0000 C CNN
F 2 "capacitor_smd:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 3438 2000 50  0001 C CNN
F 3 "~" H 3400 2150 50  0001 C CNN
	1    3400 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 1250 4100 1350
Connection ~ 4100 1250
Wire Wire Line
	3900 1250 4100 1250
Wire Wire Line
	3850 1050 4100 1050
$Comp
L Device:R_Small R8
U 1 1 61971738
P 4100 1450
F 0 "R8" H 4159 1496 50  0000 L CNN
F 1 "1.5k" H 4159 1405 50  0000 L CNN
F 2 "resistor_smd:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 4100 1450 50  0001 C CNN
F 3 "~" H 4100 1450 50  0001 C CNN
	1    4100 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R2
U 1 1 6196EA9C
P 2850 1900
F 0 "R2" V 2900 1950 50  0000 L CNN
F 1 "680k" V 2750 1800 50  0000 L CNN
F 2 "resistor_smd:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 2850 1900 50  0001 C CNN
F 3 "~" H 2850 1900 50  0001 C CNN
	1    2850 1900
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R3
U 1 1 61968966
P 3800 1250
F 0 "R3" V 3859 1296 50  0000 L CNN
F 1 "1.1" V 3850 1050 50  0000 L CNN
F 2 "resistor_smd:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 3800 1250 50  0001 C CNN
F 3 "~" H 3800 1250 50  0001 C CNN
	1    3800 1250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	900  1000 950  1000
Text GLabel 900  1000 0    50   Input ~ 0
VIN
Connection ~ 950  1150
Wire Wire Line
	950  1000 950  1150
Wire Wire Line
	1200 1150 1650 1150
Connection ~ 1200 1150
Connection ~ 1650 1150
Wire Wire Line
	950  1150 1200 1150
Wire Wire Line
	1650 1150 2000 1150
Wire Wire Line
	1650 1650 1650 1150
Wire Wire Line
	2450 1650 1650 1650
Wire Wire Line
	2450 1350 2450 1650
Wire Wire Line
	2350 1350 2450 1350
$Comp
L maguey-rescue:TD6817-emb1061 U1
U 1 1 61953D77
P 2000 1050
F 0 "U1" H 2300 1215 50  0000 C CNN
F 1 "TD6817" H 2300 1124 50  0000 C CNN
F 2 "package_to_sot_smd:TSOT-23-5" H 2300 1100 50  0001 C CNN
F 3 "" H 2300 1100 50  0001 C CNN
	1    2000 1050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 619448E9
P 1200 1300
F 0 "C2" V 1452 1300 50  0000 C CNN
F 1 "100nF" V 1361 1300 50  0000 C CNN
F 2 "capacitor_smd:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 1238 1150 50  0001 C CNN
F 3 "~" H 1200 1300 50  0001 C CNN
	1    1200 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 619448E3
P 950 1300
F 0 "C1" V 700 1300 50  0000 C CNN
F 1 "10uF" V 800 1300 50  0000 C CNN
F 2 "capacitor_smd:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 988 1150 50  0001 C CNN
F 3 "~" H 950 1300 50  0001 C CNN
	1    950  1300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J_TD1
U 1 1 619329E1
P 3750 850
F 0 "J_TD1" V 3850 1050 50  0000 R CNN
F 1 "Conn_01x02" V 3850 1100 50  0001 R CNN
F 2 "connector_pinheader_2:PinHeader_1x02_P2.54mm_Vertical" H 3750 850 50  0001 C CNN
F 3 "~" H 3750 850 50  0001 C CNN
	1    3750 850 
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 6190E93D
P 950 1900
F 0 "#PWR01" H 950 1650 50  0001 C CNN
F 1 "GND" H 955 1727 50  0000 C CNN
F 2 "" H 950 1900 50  0001 C CNN
F 3 "" H 950 1900 50  0001 C CNN
	1    950  1900
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:ESP32-C3-WROOM-02-N4 IC1
U 1 1 61C63F57
P 5950 1000
F 0 "IC1" H 6500 1300 50  0000 L CNN
F 1 "ESP32-C3-WROOM-02-N4" H 6100 1200 50  0000 L CNN
F 2 "ESP32C3WROOM02N4" H 7100 1100 50  0001 L CNN
F 3 "https://www.mouser.ch/datasheet/2/891/Espressif_ESP32_C3_WROOM_02-2006871.pdf" H 7100 1000 50  0001 L CNN
F 4 "WiFi Modules (802.11) (Engineering Samples) SMD module, ESP32-C3, 4MB SPI flash, PCB antenna, -40 C +85 C" H 7100 900 50  0001 L CNN "Description"
F 5 "3.35" H 7100 800 50  0001 L CNN "Height"
F 6 "Espressif Systems" H 7100 700 50  0001 L CNN "Manufacturer_Name"
F 7 "ESP32-C3-WROOM-02-N4" H 7100 600 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "356-ESP32C3WROOM02N4" H 7100 500 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Espressif-Systems/ESP32-C3-WROOM-02-N4?qs=stqOd1AaK7%2FqjTZKEOgfUg%3D%3D" H 7100 400 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 7100 300 50  0001 L CNN "Arrow Part Number"
F 11 "" H 7100 200 50  0001 L CNN "Arrow Price/Stock"
	1    5950 1000
	1    0    0    -1  
$EndComp
Text Notes 6150 2500 0    50   ~ 0
ESP32-C3-WROOM-02
Text Notes 700  2450 0    50   ~ 0
TD6817\nInput: 2.5 - 5.5Volt\nOutput with R1=150k & R2=680k: 3.3V\nWith jumper can be operated without uC
$Comp
L Device:R_Small R9
U 1 1 61C78A6B
P 5200 1100
F 0 "R9" V 5250 950 50  0000 C CNN
F 1 "10k" V 5250 1100 50  0000 C CNN
F 2 "resistor_smd:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 5200 1100 50  0001 C CNN
F 3 "~" H 5200 1100 50  0001 C CNN
	1    5200 1100
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C7
U 1 1 61C7A73B
P 5350 850
F 0 "C7" H 5442 896 50  0000 L CNN
F 1 "0.1uF" H 5442 805 50  0000 L CNN
F 2 "capacitor_smd:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 5350 850 50  0001 C CNN
F 3 "~" H 5350 850 50  0001 C CNN
	1    5350 850 
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C6
U 1 1 61C7B08D
P 5100 850
F 0 "C6" V 4871 850 50  0000 C CNN
F 1 "22uF" V 5250 800 50  0000 C CNN
F 2 "capacitor_tht:C_Radial_D5.0mm_H7.0mm_P2.00mm" H 5100 850 50  0001 C CNN
F 3 "~" H 5100 850 50  0001 C CNN
	1    5100 850 
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C8
U 1 1 61C7C46E
P 5350 1200
F 0 "C8" H 5258 1154 50  0000 R CNN
F 1 "0.1uF" H 5258 1245 50  0000 R CNN
F 2 "capacitor_smd:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 5350 1200 50  0001 C CNN
F 3 "~" H 5350 1200 50  0001 C CNN
	1    5350 1200
	-1   0    0    1   
$EndComp
Wire Wire Line
	5350 1000 5350 950 
Wire Wire Line
	5100 950  5100 1000
Wire Wire Line
	5100 1000 5350 1000
Connection ~ 5350 1000
Wire Wire Line
	5350 750  5350 650 
Wire Wire Line
	5350 650  5100 650 
Wire Wire Line
	5100 650  5100 750 
$Comp
L power:GND #PWR05
U 1 1 61CB9B5D
P 5100 650
F 0 "#PWR05" H 5100 400 50  0001 C CNN
F 1 "GND" V 5105 522 50  0000 R CNN
F 2 "" H 5100 650 50  0001 C CNN
F 3 "" H 5100 650 50  0001 C CNN
	1    5100 650 
	0    1    1    0   
$EndComp
Connection ~ 5100 650 
Connection ~ 5100 1000
Wire Wire Line
	5350 1100 5300 1100
$Comp
L power:GND #PWR06
U 1 1 61CE4CC5
P 5350 1300
F 0 "#PWR06" H 5350 1050 50  0001 C CNN
F 1 "GND" V 5355 1172 50  0000 R CNN
F 2 "" H 5350 1300 50  0001 C CNN
F 3 "" H 5350 1300 50  0001 C CNN
	1    5350 1300
	0    1    1    0   
$EndComp
Connection ~ 5350 1100
Wire Notes Line
	550  2850 4500 2850
Wire Notes Line
	4500 550  550  550 
Wire Notes Line
	550  550  550  2850
Text Notes 3950 2800 0    50   ~ 0
POWERSUPPLY
$Comp
L Device:D_TVS D1
U 1 1 61D8B644
P 3250 5250
F 0 "D1" V 3204 5330 50  0000 L CNN
F 1 "D_TVS" V 3295 5330 50  0000 L CNN
F 2 "SamacSys_Parts:LESD5D50CT1G" H 3250 5250 50  0001 C CNN
F 3 "~" H 3250 5250 50  0001 C CNN
	1    3250 5250
	0    1    1    0   
$EndComp
$Comp
L SamacSys_Parts:BAT760,115 D2
U 1 1 61D91CE3
P 4000 4700
F 0 "D2" H 4400 4433 50  0000 C CNN
F 1 "BAT760,115" H 4400 4524 50  0000 C CNN
F 2 "SOD2512X110N" H 4500 4850 50  0001 L CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAT760.pdf" H 4500 4750 50  0001 L CNN
F 4 "Nexperia 20V 1A, Schottky Diode, 2-Pin SOD-323 BAT760,115" H 4500 4650 50  0001 L CNN "Description"
F 5 "1.1" H 4500 4550 50  0001 L CNN "Height"
F 6 "771-BAT760-T/R" H 4500 4450 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Nexperia/BAT760115?qs=me8TqzrmIYUT08NCquDVCQ%3D%3D" H 4500 4350 50  0001 L CNN "Mouser Price/Stock"
F 8 "Nexperia" H 4500 4250 50  0001 L CNN "Manufacturer_Name"
F 9 "BAT760,115" H 4500 4150 50  0001 L CNN "Manufacturer_Part_Number"
	1    4000 4700
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R10
U 1 1 61DC0085
P 5050 5050
F 0 "R10" V 4950 4950 50  0000 C CNN
F 1 "0" V 4945 5050 50  0000 C CNN
F 2 "resistor_smd:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 5050 5050 50  0001 C CNN
F 3 "~" H 5050 5050 50  0001 C CNN
	1    5050 5050
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R11
U 1 1 61DC0A6F
P 5050 5250
F 0 "R11" V 4950 5150 50  0000 C CNN
F 1 "0" V 4945 5250 50  0000 C CNN
F 2 "resistor_smd:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 5050 5250 50  0001 C CNN
F 3 "~" H 5050 5250 50  0001 C CNN
	1    5050 5250
	0    1    1    0   
$EndComp
Wire Wire Line
	5150 5050 5350 5050
Text Notes 6150 6400 2    50   ~ 0
MICRO USB 5V&USB_UART
Wire Wire Line
	7250 1400 7250 1500
Connection ~ 7250 1500
Wire Wire Line
	7250 1500 7250 1600
Connection ~ 7250 1600
Wire Wire Line
	7250 1600 7250 1700
Connection ~ 7250 1700
Wire Wire Line
	7250 1700 7250 1800
Connection ~ 7250 1800
Wire Wire Line
	7250 1800 7250 1900
Connection ~ 7250 1900
Wire Wire Line
	7250 1900 7250 2000
Connection ~ 7250 2000
Wire Wire Line
	7250 2000 7250 2100
Connection ~ 7250 2100
Wire Wire Line
	7250 2100 7250 2200
Wire Wire Line
	5250 1800 5250 2600
Wire Wire Line
	5250 2600 7250 2600
Wire Wire Line
	7250 2600 7250 2200
Wire Wire Line
	5250 1800 5950 1800
Connection ~ 7250 2200
$Comp
L power:GND #PWR010
U 1 1 61EBA0E4
P 7250 2600
F 0 "#PWR010" H 7250 2350 50  0001 C CNN
F 1 "GND" H 7255 2427 50  0000 C CNN
F 2 "" H 7250 2600 50  0001 C CNN
F 3 "" H 7250 2600 50  0001 C CNN
	1    7250 2600
	1    0    0    -1  
$EndComp
Connection ~ 7250 2600
Wire Wire Line
	5100 1000 5100 1100
Wire Wire Line
	5350 1000 5950 1000
Wire Notes Line
	7550 2850 4650 2850
Text Notes 4700 2800 0    50   ~ 0
ESP32-Module
Wire Notes Line
	4650 550  7550 550 
Wire Notes Line
	7550 550  7550 2850
Wire Notes Line
	4650 550  4650 2850
Wire Wire Line
	9450 1600 9100 1600
$Comp
L power:GND #PWR011
U 1 1 61F22BEE
P 7750 1200
F 0 "#PWR011" H 7750 950 50  0001 C CNN
F 1 "GND" H 7755 1027 50  0000 C CNN
F 2 "" H 7750 1200 50  0001 C CNN
F 3 "" H 7750 1200 50  0001 C CNN
	1    7750 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C10
U 1 1 61F22BF4
P 8000 1200
F 0 "C10" V 8100 1100 50  0000 C CNN
F 1 "0.1uF" V 8100 1400 50  0000 C CNN
F 2 "capacitor_smd:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 8000 1200 50  0001 C CNN
F 3 "~" H 8000 1200 50  0001 C CNN
	1    8000 1200
	0    1    1    0   
$EndComp
Wire Wire Line
	7900 1200 7750 1200
Connection ~ 7750 1200
Wire Notes Line
	7650 550  10400 550 
Wire Notes Line
	10400 2300 7650 2300
$Comp
L Connector_Generic:Conn_01x10 J3
U 1 1 61F69FE5
P 8200 2950
F 0 "J3" H 8280 2942 50  0000 L CNN
F 1 "Conn_01x10" H 8280 2851 50  0000 L CNN
F 2 "connector_pinheader_2:PinHeader_1x10_P2.54mm_Vertical" H 8200 2950 50  0001 C CNN
F 3 "~" H 8200 2950 50  0001 C CNN
	1    8200 2950
	1    0    0    -1  
$EndComp
Text GLabel 9700 3500 2    50   Input ~ 0
VIN
Wire Notes Line
	7600 3700 10350 3700
Wire Notes Line
	10350 3700 10350 2350
Wire Notes Line
	10350 2350 7600 2350
Wire Notes Line
	7600 2350 7600 3700
Text Notes 9750 2250 0    50   ~ 0
SWITCH BUTTON\n
Text Notes 9800 3650 0    50   ~ 0
CONNECTOR
Wire Wire Line
	4100 1050 4100 1250
Connection ~ 4100 1050
$Comp
L Connector_Generic:Conn_01x02 J_VCC1
U 1 1 61FD780B
P 4100 850
F 0 "J_VCC1" V 4200 900 50  0000 R CNN
F 1 "Conn_01x02" V 4200 1100 50  0001 R CNN
F 2 "connector_pinheader_2:PinHeader_1x02_P2.54mm_Vertical" H 4100 850 50  0001 C CNN
F 3 "~" H 4100 850 50  0001 C CNN
	1    4100 850 
	0    -1   -1   0   
$EndComp
Wire Notes Line
	4500 2850 4500 550 
Text Notes 600  2800 0    50   ~ 0
J_TD6817: Normally: set Jumper. To see if TD6817 works , remove jumper.\nJ_VCC: Normally: set Jumper. Without Jumper 3.3V is not available.
$Comp
L Device:LED D3
U 1 1 6212E5F5
P 4100 1850
F 0 "D3" H 4200 1800 50  0000 C CNN
F 1 "LED" H 4250 1900 50  0000 C CNN
F 2 "diode_smd:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4100 1850 50  0001 C CNN
F 3 "~" H 4100 1850 50  0001 C CNN
	1    4100 1850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4100 1550 4100 1700
Wire Wire Line
	4100 2000 4100 2350
Wire Wire Line
	3250 5100 3250 4700
Wire Wire Line
	3300 4700 3250 4700
Connection ~ 3250 4700
$Comp
L SamacSys_Parts:LL3301AF065QJ S1
U 1 1 61A3EF45
P 7800 850
F 0 "S1" H 8400 1115 50  0000 C CNN
F 1 "LL3301AF065QJ" H 8400 1024 50  0000 C CNN
F 2 "LL3301FF065QJ" H 8850 950 50  0001 L CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/E-Switch%20PDFs/LL3301AF065QJ.pdf" H 8850 850 50  0001 L CNN
F 4 "Pushbutton Switches 50mA 12VDC F065 5.0mm J-Lead" H 8850 750 50  0001 L CNN "Description"
F 5 "7.3" H 8850 650 50  0001 L CNN "Height"
F 6 "612-LL3301AF065QJ" H 8850 550 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/E-Switch/LL3301AF065QJ?qs=QtyuwXswaQi68RkbBY57jQ%3D%3D" H 8850 450 50  0001 L CNN "Mouser Price/Stock"
F 8 "E-Switch" H 8850 350 50  0001 L CNN "Manufacturer_Name"
F 9 "LL3301AF065QJ" H 8850 250 50  0001 L CNN "Manufacturer_Part_Number"
	1    7800 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 850  9100 850 
Wire Wire Line
	9100 850  9100 1200
Wire Wire Line
	8100 1200 9100 1200
Connection ~ 9100 850 
Wire Wire Line
	9100 850  9450 850 
Wire Wire Line
	7800 850  7750 850 
Wire Wire Line
	7750 850  7750 1200
$Comp
L power:GND #PWR012
U 1 1 61AC1439
P 7750 1950
F 0 "#PWR012" H 7750 1700 50  0001 C CNN
F 1 "GND" H 7755 1777 50  0000 C CNN
F 2 "" H 7750 1950 50  0001 C CNN
F 3 "" H 7750 1950 50  0001 C CNN
	1    7750 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C11
U 1 1 61AC143F
P 8000 1950
F 0 "C11" V 8100 1850 50  0000 C CNN
F 1 "0.1uF" V 8100 2150 50  0000 C CNN
F 2 "capacitor_smd:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 8000 1950 50  0001 C CNN
F 3 "~" H 8000 1950 50  0001 C CNN
	1    8000 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	7900 1950 7750 1950
Connection ~ 7750 1950
$Comp
L SamacSys_Parts:LL3301AF065QJ S2
U 1 1 61AC144D
P 7800 1600
F 0 "S2" H 8400 1865 50  0000 C CNN
F 1 "LL3301AF065QJ" H 8400 1774 50  0000 C CNN
F 2 "LL3301FF065QJ" H 8850 1700 50  0001 L CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/E-Switch%20PDFs/LL3301AF065QJ.pdf" H 8850 1600 50  0001 L CNN
F 4 "Pushbutton Switches 50mA 12VDC F065 5.0mm J-Lead" H 8850 1500 50  0001 L CNN "Description"
F 5 "7.3" H 8850 1400 50  0001 L CNN "Height"
F 6 "612-LL3301AF065QJ" H 8850 1300 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/E-Switch/LL3301AF065QJ?qs=QtyuwXswaQi68RkbBY57jQ%3D%3D" H 8850 1200 50  0001 L CNN "Mouser Price/Stock"
F 8 "E-Switch" H 8850 1100 50  0001 L CNN "Manufacturer_Name"
F 9 "LL3301AF065QJ" H 8850 1000 50  0001 L CNN "Manufacturer_Part_Number"
	1    7800 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 1600 9100 1600
Wire Wire Line
	9100 1600 9100 1950
Wire Wire Line
	8100 1950 9100 1950
Connection ~ 9100 1600
Wire Wire Line
	7800 1600 7750 1600
Wire Wire Line
	7750 1600 7750 1950
Wire Notes Line
	10400 550  10400 2300
Wire Notes Line
	7650 550  7650 2300
NoConn ~ 7800 1700
NoConn ~ 9000 1700
NoConn ~ 9000 950 
NoConn ~ 7800 950 
Text GLabel 4150 4700 2    50   Input ~ 0
DC+
Text GLabel 4350 5250 0    50   Input ~ 0
RXD
Text GLabel 4350 5050 0    50   Input ~ 0
TXD
Text GLabel 5800 5250 2    50   Input ~ 0
TXD0_P
Text GLabel 5800 5050 2    50   Input ~ 0
RXD0_N
Text GLabel 5950 2100 0    50   Input ~ 0
TXD0_P
Text GLabel 5950 2000 0    50   Input ~ 0
RXD0_N
Text GLabel 9450 850  2    50   Input ~ 0
EN
Text GLabel 5700 1150 3    50   Input ~ 0
EN
Wire Wire Line
	5700 1100 5700 1150
Wire Wire Line
	5350 1100 5700 1100
Wire Wire Line
	5700 1100 5950 1100
Connection ~ 5700 1100
Text GLabel 9450 1600 2    50   Input ~ 0
IO0
Text GLabel 7250 1300 2    50   Input ~ 0
IO0
Text GLabel 4250 1050 2    50   Input ~ 0
VCC
Text GLabel 5100 1000 0    50   Input ~ 0
VCC
Wire Wire Line
	4200 1050 4250 1050
Text GLabel 7950 2550 0    50   Input ~ 0
VCC
Wire Wire Line
	8000 2550 7950 2550
Text GLabel 9650 2600 2    50   Input ~ 0
IO0
Wire Wire Line
	9650 3400 9700 3400
Wire Wire Line
	9650 3500 9700 3500
Text GLabel 5950 1200 0    50   Input ~ 0
IO4
Text GLabel 5950 1300 0    50   Input ~ 0
IO5
Text GLabel 5950 1400 0    50   Input ~ 0
IO6
Text GLabel 5950 1500 0    50   Input ~ 0
IO7
Text GLabel 5950 1600 0    50   Input ~ 0
IO8
Text GLabel 5950 1700 0    50   Input ~ 0
IO9
Text GLabel 5950 2200 0    50   Input ~ 0
IO18
Text GLabel 5950 2300 0    50   Input ~ 0
IO19
Text GLabel 7250 1200 2    50   Input ~ 0
IO1
Text GLabel 7250 1100 2    50   Input ~ 0
IO2
Text GLabel 7250 1000 2    50   Input ~ 0
IO3
Text GLabel 9650 2700 2    50   Input ~ 0
IO1
Text GLabel 9650 2800 2    50   Input ~ 0
IO2
Text GLabel 9650 2900 2    50   Input ~ 0
IO3
Text GLabel 8000 2650 0    50   Input ~ 0
IO4
Text GLabel 8000 2750 0    50   Input ~ 0
IO5
Text GLabel 8000 2850 0    50   Input ~ 0
IO6
Text GLabel 8000 2950 0    50   Input ~ 0
IO7
Text GLabel 8000 3050 0    50   Input ~ 0
IO8
Text GLabel 8000 3150 0    50   Input ~ 0
IO9
Text GLabel 5950 1900 0    50   Input ~ 0
IO10
Text GLabel 9650 3000 2    50   Input ~ 0
IO19
Text GLabel 9650 3100 2    50   Input ~ 0
IO18
Text GLabel 9650 3200 2    50   Input ~ 0
IO10
$Comp
L Connector_Generic:Conn_01x10 J4
U 1 1 61F6AC29
P 9450 3000
F 0 "J4" H 9368 3617 50  0000 C CNN
F 1 "Conn_01x10" H 9368 3526 50  0000 C CNN
F 2 "connector_pinheader_2:PinHeader_1x10_P2.54mm_Vertical" H 9450 3000 50  0001 C CNN
F 3 "~" H 9450 3000 50  0001 C CNN
	1    9450 3000
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 61F97FDD
P 9700 3400
F 0 "#PWR014" H 9700 3150 50  0001 C CNN
F 1 "GND" V 9705 3272 50  0000 R CNN
F 2 "" H 9700 3400 50  0001 C CNN
F 3 "" H 9700 3400 50  0001 C CNN
	1    9700 3400
	0    -1   1    0   
$EndComp
NoConn ~ 9650 3300
$Comp
L Mechanical:MountingHole H1
U 1 1 61996F8B
P 10900 1850
F 0 "H1" H 11000 1896 50  0000 L CNN
F 1 "MountingHole" H 11000 1805 50  0000 L CNN
F 2 "mountinghole:MountingHole_2.2mm_M2" H 10900 1850 50  0001 C CNN
F 3 "~" H 10900 1850 50  0001 C CNN
	1    10900 1850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 61997434
P 10900 2050
F 0 "H2" H 11000 2096 50  0000 L CNN
F 1 "MountingHole" H 11000 2005 50  0000 L CNN
F 2 "mountinghole:MountingHole_2.2mm_M2" H 10900 2050 50  0001 C CNN
F 3 "~" H 10900 2050 50  0001 C CNN
	1    10900 2050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 61998413
P 10900 2250
F 0 "H3" H 11000 2296 50  0000 L CNN
F 1 "MountingHole" H 11000 2205 50  0000 L CNN
F 2 "mountinghole:MountingHole_2.2mm_M2" H 10900 2250 50  0001 C CNN
F 3 "~" H 10900 2250 50  0001 C CNN
	1    10900 2250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 61998F32
P 10900 2450
F 0 "H4" H 11000 2496 50  0000 L CNN
F 1 "MountingHole" H 11000 2405 50  0000 L CNN
F 2 "mountinghole:MountingHole_2.2mm_M2" H 10900 2450 50  0001 C CNN
F 3 "~" H 10900 2450 50  0001 C CNN
	1    10900 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R4
U 1 1 61A0074D
P 3800 1350
F 0 "R4" V 3859 1396 50  0000 L CNN
F 1 "1.1" V 3850 1150 50  0000 L CNN
F 2 "resistor_smd:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 3800 1350 50  0001 C CNN
F 3 "~" H 3800 1350 50  0001 C CNN
	1    3800 1350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R5
U 1 1 61A010CD
P 3800 1450
F 0 "R5" V 3859 1496 50  0000 L CNN
F 1 "1.1" V 3850 1250 50  0000 L CNN
F 2 "resistor_smd:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 3800 1450 50  0001 C CNN
F 3 "~" H 3800 1450 50  0001 C CNN
	1    3800 1450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R6
U 1 1 61A019A3
P 3800 1550
F 0 "R6" V 3859 1596 50  0000 L CNN
F 1 "1.1" V 3850 1350 50  0000 L CNN
F 2 "resistor_smd:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 3800 1550 50  0001 C CNN
F 3 "~" H 3800 1550 50  0001 C CNN
	1    3800 1550
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R7
U 1 1 61A022E7
P 3800 1650
F 0 "R7" V 3859 1696 50  0000 L CNN
F 1 "1.1" V 3850 1450 50  0000 L CNN
F 2 "resistor_smd:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 3800 1650 50  0001 C CNN
F 3 "~" H 3800 1650 50  0001 C CNN
	1    3800 1650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3900 1650 3900 1550
Connection ~ 3900 1250
Connection ~ 3900 1350
Wire Wire Line
	3900 1350 3900 1250
Connection ~ 3900 1450
Wire Wire Line
	3900 1450 3900 1350
Connection ~ 3900 1550
Wire Wire Line
	3900 1550 3900 1450
Wire Wire Line
	3700 1250 3700 1350
Connection ~ 3700 1250
Connection ~ 3700 1350
Wire Wire Line
	3700 1350 3700 1450
Connection ~ 3700 1450
Wire Wire Line
	3700 1450 3700 1550
Connection ~ 3700 1550
Wire Wire Line
	3700 1550 3700 1650
Wire Wire Line
	3400 1250 3700 1250
$Comp
L Device:LED D8
U 1 1 61A5A7F8
P 7500 5350
F 0 "D8" V 7539 5232 50  0000 R CNN
F 1 "YELLOW" V 7448 5232 50  0000 R CNN
F 2 "diode_smd:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7500 5350 50  0001 C CNN
F 3 "~" H 7500 5350 50  0001 C CNN
	1    7500 5350
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D7
U 1 1 61A5B4E4
P 7200 5350
F 0 "D7" V 7239 5232 50  0000 R CNN
F 1 "GREEN" V 7148 5232 50  0000 R CNN
F 2 "diode_smd:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7200 5350 50  0001 C CNN
F 3 "~" H 7200 5350 50  0001 C CNN
	1    7200 5350
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D6
U 1 1 61A5BEB1
P 6900 5350
F 0 "D6" V 6939 5232 50  0000 R CNN
F 1 "RED" V 6848 5232 50  0000 R CNN
F 2 "diode_smd:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6900 5350 50  0001 C CNN
F 3 "~" H 6900 5350 50  0001 C CNN
	1    6900 5350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R13
U 1 1 61A5CE5E
P 7200 5600
F 0 "R13" V 7004 5600 50  0000 C CNN
F 1 "1.5k" V 7095 5600 50  0000 C CNN
F 2 "resistor_smd:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 7200 5600 50  0001 C CNN
F 3 "~" H 7200 5600 50  0001 C CNN
	1    7200 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R12
U 1 1 61A5D77B
P 6900 5600
F 0 "R12" V 6704 5600 50  0000 C CNN
F 1 "1.5k" V 6795 5600 50  0000 C CNN
F 2 "resistor_smd:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 6900 5600 50  0001 C CNN
F 3 "~" H 6900 5600 50  0001 C CNN
	1    6900 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R14
U 1 1 61A5E09D
P 7500 5600
F 0 "R14" V 7304 5600 50  0000 C CNN
F 1 "1.5k" V 7395 5600 50  0000 C CNN
F 2 "resistor_smd:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 7500 5600 50  0001 C CNN
F 3 "~" H 7500 5600 50  0001 C CNN
	1    7500 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R_PROG1
U 1 1 61A5EE2F
P 8900 5950
F 0 "R_PROG1" V 9000 5950 50  0000 C CNN
F 1 "1k" V 8795 5950 50  0000 C CNN
F 2 "resistor_smd:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 8900 5950 50  0001 C CNN
F 3 "~" H 8900 5950 50  0001 C CNN
	1    8900 5950
	0    1    1    0   
$EndComp
Wire Wire Line
	8100 5300 8100 5200
Text GLabel 6700 5200 0    50   Input ~ 0
DC+
Wire Wire Line
	6900 5200 7200 5200
Connection ~ 6900 5200
Connection ~ 7200 5200
Wire Wire Line
	7200 5200 7500 5200
Wire Wire Line
	7500 5200 8100 5200
Connection ~ 7500 5200
Connection ~ 8100 5200
Wire Wire Line
	7500 5700 7500 5750
Wire Wire Line
	7500 5750 7900 5750
Wire Wire Line
	7900 5750 7900 5400
Wire Wire Line
	7900 5400 8100 5400
Wire Wire Line
	8100 5500 7950 5500
Wire Wire Line
	7950 5500 7950 5800
Wire Wire Line
	7950 5800 7200 5800
Wire Wire Line
	7200 5800 7200 5700
$Comp
L power:GND #PWR013
U 1 1 61AF8087
P 8000 6050
F 0 "#PWR013" H 8000 5800 50  0001 C CNN
F 1 "GND" H 8005 5877 50  0000 C CNN
F 2 "" H 8000 6050 50  0001 C CNN
F 3 "" H 8000 6050 50  0001 C CNN
	1    8000 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 5600 9600 5600
Wire Wire Line
	9600 5600 9600 5950
Wire Wire Line
	9600 5950 9000 5950
Wire Wire Line
	8100 5600 8000 5600
Wire Wire Line
	8000 5600 8000 5950
Wire Wire Line
	8800 5950 8000 5950
Connection ~ 8000 5950
Wire Wire Line
	8000 5950 8000 6050
Wire Wire Line
	9500 5500 9650 5500
Wire Wire Line
	9650 5500 9650 6300
Wire Wire Line
	6900 6300 6900 5700
Wire Wire Line
	9500 5300 9500 5200
Connection ~ 9500 5200
$Comp
L Device:C_Small C9
U 1 1 61B35385
P 6750 5300
F 0 "C9" V 6521 5300 50  0000 C CNN
F 1 "10uF" V 6850 5350 50  0000 C CNN
F 2 "capacitor_tht:C_Radial_D5.0mm_H7.0mm_P2.00mm" H 6750 5300 50  0001 C CNN
F 3 "~" H 6750 5300 50  0001 C CNN
	1    6750 5300
	-1   0    0    1   
$EndComp
Wire Wire Line
	6700 5200 6750 5200
Connection ~ 6750 5200
$Comp
L power:GND #PWR09
U 1 1 61B3FAD0
P 6750 5400
F 0 "#PWR09" H 6750 5150 50  0001 C CNN
F 1 "GND" H 6755 5227 50  0000 C CNN
F 2 "" H 6750 5400 50  0001 C CNN
F 3 "" H 6750 5400 50  0001 C CNN
	1    6750 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C12
U 1 1 61B406DA
P 9850 5300
F 0 "C12" V 9800 5200 50  0000 C CNN
F 1 "10uF" V 10000 5250 50  0000 C CNN
F 2 "capacitor_tht:C_Radial_D5.0mm_H7.0mm_P2.00mm" H 9850 5300 50  0001 C CNN
F 3 "~" H 9850 5300 50  0001 C CNN
	1    9850 5300
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR017
U 1 1 61B40CF8
P 9850 5400
F 0 "#PWR017" H 9850 5150 50  0001 C CNN
F 1 "GND" H 9855 5227 50  0000 C CNN
F 2 "" H 9850 5400 50  0001 C CNN
F 3 "" H 9850 5400 50  0001 C CNN
	1    9850 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R15
U 1 1 61B5B20D
P 9800 5900
F 0 "R15" V 9604 5900 50  0000 C CNN
F 1 "10k" V 9695 5900 50  0000 C CNN
F 2 "resistor_smd:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 9800 5900 50  0001 C CNN
F 3 "~" H 9800 5900 50  0001 C CNN
	1    9800 5900
	-1   0    0    1   
$EndComp
Wire Wire Line
	9500 5400 9800 5400
Wire Wire Line
	9650 6300 6900 6300
$Comp
L power:GND #PWR016
U 1 1 61B6E2F5
P 9800 6050
F 0 "#PWR016" H 9800 5800 50  0001 C CNN
F 1 "GND" H 9805 5877 50  0000 C CNN
F 2 "" H 9800 6050 50  0001 C CNN
F 3 "" H 9800 6050 50  0001 C CNN
	1    9800 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 6000 9800 6050
$Comp
L Connector_Generic:Conn_01x02 JVBAT1
U 1 1 61B78CF9
P 10300 3950
F 0 "JVBAT1" V 10400 4150 50  0000 R CNN
F 1 "Conn_01x02" V 10400 4200 50  0001 R CNN
F 2 "connector_pinheader_2:PinHeader_1x02_P2.54mm_Vertical" H 10300 3950 50  0001 C CNN
F 3 "~" H 10300 3950 50  0001 C CNN
	1    10300 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10300 5400 10400 5400
$Comp
L Connector_Generic:Conn_01x03 J5
U 1 1 61B83782
P 10600 5300
F 0 "J5" H 10680 5342 50  0000 L CNN
F 1 "Conn_01x03" H 10400 5000 50  0000 L CNN
F 2 "connector_jst:JST_GH_SM03B-GHS-TB_1x03-1MP_P1.25mm_Horizontal" H 10600 5300 50  0001 C CNN
F 3 "~" H 10600 5300 50  0001 C CNN
	1    10600 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	10400 5300 10200 5300
Wire Wire Line
	9800 5400 9800 5750
Wire Wire Line
	10200 5750 9800 5750
Wire Wire Line
	10200 5300 10200 5750
Connection ~ 9800 5750
Wire Wire Line
	9800 5750 9800 5800
Wire Wire Line
	10300 5400 10300 5500
Text Notes 10150 6200 0    50   ~ 0
Battery type: \n3.7V/2000mAh 
Text Notes 8300 6200 0    50   ~ 0
R_PROG: 1.00k := I_REG: 1000mA
$Comp
L Transistor_FET:AO3401A Q1
U 1 1 61B68B23
P 10350 4850
F 0 "Q1" H 10555 4804 50  0000 L CNN
F 1 "AO3401A" H 10500 5000 50  0000 L CNN
F 2 "package_to_sot_smd:SOT-23" H 10550 4775 50  0001 L CIN
F 3 "http://www.aosmd.com/pdfs/datasheet/AO3401A.pdf" H 10350 4850 50  0001 L CNN
	1    10350 4850
	1    0    0    1   
$EndComp
Wire Wire Line
	9500 4750 9550 4750
Wire Wire Line
	9550 4550 9550 4200
Connection ~ 9850 5200
Wire Wire Line
	9500 4750 9500 5200
$Comp
L Device:D_Schottky D9
U 1 1 61C824FD
P 8800 4150
F 0 "D9" H 8800 3933 50  0000 C CNN
F 1 "D_Schottky" H 8800 4024 50  0000 C CNN
F 2 "resistor_tht:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" H 8800 4150 50  0001 C CNN
F 3 "~" H 8800 4150 50  0001 C CNN
	1    8800 4150
	-1   0    0    1   
$EndComp
Wire Wire Line
	8100 5200 8100 4150
Wire Wire Line
	8950 4200 8950 4150
Wire Wire Line
	8950 4150 10300 4150
Connection ~ 8950 4150
Text GLabel 10500 4150 2    50   Input ~ 0
VIN
Wire Wire Line
	10400 4150 10500 4150
$Comp
L power:GND #PWR018
U 1 1 61CBB3B6
P 10300 5500
F 0 "#PWR018" H 10300 5250 50  0001 C CNN
F 1 "GND" H 10305 5327 50  0000 C CNN
F 2 "" H 10300 5500 50  0001 C CNN
F 3 "" H 10300 5500 50  0001 C CNN
	1    10300 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 4150 8650 4150
Connection ~ 9550 4200
Wire Wire Line
	9550 4200 8950 4200
Wire Wire Line
	9550 4200 9750 4200
Wire Wire Line
	9500 5200 9850 5200
Wire Wire Line
	9850 5200 10350 5200
Wire Wire Line
	10350 5200 10350 5050
Wire Wire Line
	10350 5050 10450 5050
Connection ~ 10350 5200
Wire Wire Line
	10350 5200 10400 5200
Wire Wire Line
	10450 4200 10450 4650
Wire Wire Line
	10150 4650 10150 4850
Wire Wire Line
	6750 5200 6900 5200
Wire Notes Line
	10900 6450 10900 3750
Wire Notes Line
	10900 3750 6450 3750
Wire Notes Line
	10900 6450 6450 6450
Wire Notes Line
	6450 3750 6450 6450
Text Notes 10200 6450 0    50   ~ 0
BATTERY CHARGER
Text Notes 6550 3900 0    50   ~ 0
J_VBAT: Normally: set Jumper. Check voltages of battery charger/power path\n
$Comp
L power:GND #PWR03
U 1 1 61C4854C
P 2600 4800
F 0 "#PWR03" H 2600 4550 50  0001 C CNN
F 1 "GND" H 2605 4627 50  0000 C CNN
F 2 "" H 2600 4800 50  0001 C CNN
F 3 "" H 2600 4800 50  0001 C CNN
	1    2600 4800
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR04
U 1 1 61C5543C
P 3250 5400
F 0 "#PWR04" H 3250 5150 50  0001 C CNN
F 1 "GND" H 3255 5227 50  0000 C CNN
F 2 "" H 3250 5400 50  0001 C CNN
F 3 "" H 3250 5400 50  0001 C CNN
	1    3250 5400
	1    0    0    -1  
$EndComp
Text GLabel 2600 4100 2    50   Input ~ 0
RXD
Text GLabel 2600 4200 2    50   Input ~ 0
TXD
$Comp
L Device:D_TVS D5
U 1 1 61C98AFD
P 5350 5400
F 0 "D5" V 5304 5480 50  0000 L CNN
F 1 "D_TVS" V 5395 5480 50  0000 L CNN
F 2 "SamacSys_Parts:LESD5D50CT1G" H 5350 5400 50  0001 C CNN
F 3 "~" H 5350 5400 50  0001 C CNN
	1    5350 5400
	0    1    1    0   
$EndComp
$Comp
L Device:D_TVS D4
U 1 1 61C99B44
P 5350 4900
F 0 "D4" V 5304 4980 50  0000 L CNN
F 1 "D_TVS" V 5395 4980 50  0000 L CNN
F 2 "SamacSys_Parts:LESD5D50CT1G" H 5350 4900 50  0001 C CNN
F 3 "~" H 5350 4900 50  0001 C CNN
	1    5350 4900
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR08
U 1 1 61CAA61C
P 5350 5550
F 0 "#PWR08" H 5350 5300 50  0001 C CNN
F 1 "GND" H 5355 5377 50  0000 C CNN
F 2 "" H 5350 5550 50  0001 C CNN
F 3 "" H 5350 5550 50  0001 C CNN
	1    5350 5550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 61CAB1BB
P 5350 4750
F 0 "#PWR07" H 5350 4500 50  0001 C CNN
F 1 "GND" H 5355 4577 50  0000 C CNN
F 2 "" H 5350 4750 50  0001 C CNN
F 3 "" H 5350 4750 50  0001 C CNN
	1    5350 4750
	-1   0    0    1   
$EndComp
Text Notes 4400 5400 0    50   ~ 0
3.3V UART only!
Wire Notes Line
	1050 6450 1050 3850
Wire Notes Line
	1050 3850 6250 3850
Wire Notes Line
	6250 3850 6250 6450
Wire Notes Line
	6250 6450 1050 6450
Connection ~ 5350 5050
Wire Wire Line
	5350 5050 5800 5050
Wire Wire Line
	4350 5050 4950 5050
Wire Wire Line
	4350 5250 4950 5250
Connection ~ 5350 5250
Wire Wire Line
	5350 5250 5800 5250
Wire Wire Line
	5150 5250 5350 5250
NoConn ~ 8000 3250
NoConn ~ 8000 3350
NoConn ~ 8000 3450
Wire Wire Line
	3900 4700 4000 4700
$Comp
L Connector_Generic:Conn_01x02 JPOW1
U 1 1 61DDB67F
P 4000 4500
F 0 "JPOW1" V 4100 4700 50  0000 R CNN
F 1 "Conn_01x02" V 4100 4750 50  0001 R CNN
F 2 "connector_pinheader_2:PinHeader_1x02_P2.54mm_Vertical" H 4000 4500 50  0001 C CNN
F 3 "~" H 4000 4500 50  0001 C CNN
	1    4000 4500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4100 4700 4150 4700
Text Notes 2700 6050 0    50   ~ 0
J_POW: Normally: set Jumper. General power supply of board\n
Wire Wire Line
	2600 4700 2950 4700
$Comp
L Amplifier_Operational:MCP601R U2
U 1 1 61E945EB
P 9850 4650
F 0 "U2" H 10194 4696 50  0000 L CNN
F 1 "MCP601R" H 10194 4605 50  0000 L CNN
F 2 "package_to_sot_smd:SOT-223-5" H 9850 4650 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21314g.pdf" H 9850 4850 50  0001 C CNN
	1    9850 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 4350 9750 4200
Connection ~ 9750 4200
Wire Wire Line
	9750 4200 10450 4200
$Comp
L power:GND #PWR015
U 1 1 61EA6659
P 9750 4950
F 0 "#PWR015" H 9750 4700 50  0001 C CNN
F 1 "GND" H 9755 4777 50  0000 C CNN
F 2 "" H 9750 4950 50  0001 C CNN
F 3 "" H 9750 4950 50  0001 C CNN
	1    9750 4950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J2
U 1 1 61ED0497
P 2400 4900
F 0 "J2" H 2318 5317 50  0000 C CNN
F 1 "Conn_01x06" H 2318 5226 50  0000 C CNN
F 2 "connector_pinheader_2:PinHeader_1x06_P2.54mm_Vertical" H 2400 4900 50  0001 C CNN
F 3 "~" H 2400 4900 50  0001 C CNN
	1    2400 4900
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 61F46477
P 2400 4200
F 0 "J1" H 2318 4517 50  0000 C CNN
F 1 "Conn_01x04" H 2318 4426 50  0000 C CNN
F 2 "connector_pinsocket_2:PinSocket_1x04_P2.54mm_Vertical" H 2400 4200 50  0001 C CNN
F 3 "~" H 2400 4200 50  0001 C CNN
	1    2400 4200
	-1   0    0    -1  
$EndComp
Connection ~ 2950 4700
Wire Wire Line
	2950 4700 3250 4700
$Comp
L power:GND #PWR019
U 1 1 61F5C415
P 2600 4400
F 0 "#PWR019" H 2600 4150 50  0001 C CNN
F 1 "GND" H 2605 4227 50  0000 C CNN
F 2 "" H 2600 4400 50  0001 C CNN
F 3 "" H 2600 4400 50  0001 C CNN
	1    2600 4400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2600 4300 2950 4300
Wire Wire Line
	2950 4300 2950 4700
Text Notes 1250 5450 0    50   ~ 0
Connected to J2:\nUSB-C-connector \nhttps://www.sparkfun.com/products/15100
$Comp
L Connector_Generic:Conn_01x05 J6
U 1 1 61C023E1
P 8300 5400
F 0 "J6" H 8250 5850 50  0000 L CNN
F 1 "Conn_01x05" H 8150 5750 50  0000 L CNN
F 2 "connector_pinheader_2:PinHeader_1x05_P2.54mm_Vertical" H 8300 5400 50  0001 C CNN
F 3 "~" H 8300 5400 50  0001 C CNN
	1    8300 5400
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x05 J7
U 1 1 61C02D63
P 9300 5400
F 0 "J7" H 9218 4975 50  0000 C CNN
F 1 "Conn_01x05" H 9218 5066 50  0000 C CNN
F 2 "connector_pinheader_2:PinHeader_1x05_P2.54mm_Vertical" H 9300 5400 50  0001 C CNN
F 3 "~" H 9300 5400 50  0001 C CNN
	1    9300 5400
	-1   0    0    1   
$EndComp
Wire Notes Line
	8300 5150 8300 5750
Wire Notes Line
	8300 5750 9300 5750
Wire Notes Line
	9300 5750 9300 5150
Wire Notes Line
	9300 5150 8300 5150
Text Notes 8450 5150 0    50   ~ 0
MCP73833-AMI_UN
Text Notes 8350 5600 0    50   ~ 0
VDD_1\nVDD_2\nSTAT1\nSTAT2\nVSS
Text Notes 9000 5600 0    50   ~ 0
VBAT_1\nVBAT_2\nTHERM\n!PG(TE)\nPROG
Wire Notes Line
	2350 4650 2350 5250
Wire Notes Line
	2350 5250 1950 5250
Wire Notes Line
	1950 5250 1950 4650
Wire Notes Line
	1950 4650 2350 4650
Text Notes 2100 5250 0    59   ~ 0
VBUS\nGND\nCC1\nD-\nD+\nCC2
$EndSCHEMATC
